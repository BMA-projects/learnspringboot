package com.in28minutes.springBoot.learnspringboot;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class CourseController {
    /**
     * Objectif : Récuperer une liste de cours et renvoyer leur id , nom et auteur avec l url "/courses"
     */
    @RequestMapping("/courses")
    public List<Course> retrieveAllCourses() {
        return Arrays.asList(
                new Course(1, "Learn AWS", "in28minutes"),
                new Course(1, "Learn react", "in28minutes"),
                new Course(1, "Learn jpa", "in28minutes")
                );
    }
}
